/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

/**
 *
 * @author ARCarrero
 */
public class Ventas {
    
    private int idArticle;
    private String nameArticle;
    private int idListPrice;
    private float price;
    private int order;
    private int idBill;
    private int cantMax;
    private int payMode;
    private String urlImage;

    public Ventas(int idArticle, String nameArticle, int idListPrice, float price, int order, int idBill) {
        this.idArticle = idArticle;
        this.nameArticle = nameArticle;
        this.idListPrice = idListPrice;
        this.price = price;
        this.order = order;
        this.idBill = idBill;
    }

    public Ventas() {
        idArticle=0;
        nameArticle="";
        idListPrice=0;
        price = 0;
        order = 0;
        idBill = 0;
        cantMax = 0;
        payMode = 0;
        urlImage = "";
    }

    public int getIdArticle() {
        return idArticle;
    }

    public void setIdArticle(int idArticle) {
        this.idArticle = idArticle;
    }

    public String getNameArticle() {
        return nameArticle;
    }

    public void setNameArticle(String nameArticle) {
        this.nameArticle = nameArticle;
    }

    public int getIdListPrice() {
        return idListPrice;
    }

    public void setIdListPrice(int idListPrice) {
        this.idListPrice = idListPrice;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public int getIdBill() {
        return idBill;
    }

    public void setIdBill(int idBill) {
        this.idBill = idBill;
    }

    public int getCantMax() {
        return cantMax;
    }

    public void setCantMax(int cantMax) {
        this.cantMax = cantMax;
    }

    public int getPayMode() {
        return payMode;
    }

    public void setPayMode(int payMode) {
        this.payMode = payMode;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }
    
}
