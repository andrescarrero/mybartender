package Controllers;

import Models.Entities.ListPrice;
import Controllers.util.JsfUtil;
import Controllers.util.JsfUtil.PersistAction;
import Models.Entities.Article;
import Models.Entities.ListPricePK;
import Models.Queries.ListPriceFacade;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.naming.InitialContext;
import javax.transaction.UserTransaction;
import org.primefaces.context.RequestContext;

@Named("listPriceController")
@SessionScoped
public class ListPriceController implements Serializable {

    @EJB
    private Models.Queries.ListPriceFacade ejbFacade;
    private List<ListPrice> items = null;
    private ListPrice selected;
    private ListPrice newListPrice;
    private Controllers.ArticleController articleController;
    private List<ListPrice> listado;
    private Date tomorrow;
    private Date today;

    public ListPriceController() {
    }

    public ListPrice getSelected() {
        return selected;
    }

    public void setSelected(ListPrice selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
        selected.getListPricePK().setIdArticle(selected.getArticle().getIdArticle());
    }

    protected void initializeEmbeddableKey() {
        selected.setListPricePK(new Models.Entities.ListPricePK());
    }

    private ListPriceFacade getFacade() {
        return ejbFacade;
    }

    public ListPrice prepareCreate() {
        selected = new ListPrice();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("ListPriceCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        java.util.Date fecha = new Date();
        boolean band = true;
        String msj = "";

        //Fechas mayores al día actual (Por política de negocio), los precios no cambian durante el día.
        if (selected.getDatefin().before(fecha)) {
            band = false;
            msj = "The final and initial date must be higher than today";
        }

        //Fecha final mayor o igual a fecha inicial.
        if (band && selected.getDatefin().before(selected.getDateini())) {
            band = false;
            msj = "Change Final Date, Must be greater than or equal to the Initial Date";
        }

        //Se consulta que la fecha final no esté dentro de otro rango de precio definido, si es así no se permite hacer la actualización.
        ListPrice lastItemDate = ejbFacade.buscarListaPrecioSolapada(selected, selected.getDatefin());
        if (band && lastItemDate != null) {

            //Si la fecha final es igual o mayor al registro encontrado.
            if (!selected.getDatefin().before(lastItemDate.getDateini())) {
                band = false;
                DateFormat day = new SimpleDateFormat("dd/MM/YYYY");
                String dateConverter = day.format(lastItemDate.getDateini());
                msj = "Change Final Date, There is overlap between records, the end date can not be equal to or greater than " + dateConverter;
            }

        }

        if (band) {
            UserTransaction transaction = null;
            try {
                transaction = (UserTransaction) new InitialContext().lookup("java:comp/UserTransaction");
                transaction.begin();

                persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("ListPriceUpdated"));
                transaction.commit();
                RequestContext confirm = RequestContext.getCurrentInstance();
                confirm.execute("PF('ListPriceEditDialog').hide();");
            } catch (Exception e) {
                if (transaction != null) {
                    try {
                        transaction.rollback();
                    } catch (Exception ex) {
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"), ""));
                    }
                }
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"), ""));
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, msj, ""));
        }

    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("ListPriceDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<ListPrice> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    java.util.Date fecha = new Date();
                    selected.setDateEdit(fecha);
                    selected.setUserEdit("arcarrero");
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public ListPrice getListPrice(Models.Entities.ListPricePK id) {
        return getFacade().find(id);
    }

    public List<ListPrice> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<ListPrice> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = ListPrice.class)
    public static class ListPriceControllerConverter implements Converter {

        private static final String SEPARATOR = "#";
        private static final String SEPARATOR_ESCAPED = "\\#";

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            ListPriceController controller = (ListPriceController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "listPriceController");
            return controller.getListPrice(getKey(value));
        }

        Models.Entities.ListPricePK getKey(String value) {
            Models.Entities.ListPricePK key;
            String values[] = value.split(SEPARATOR_ESCAPED);
            key = new Models.Entities.ListPricePK();
            key.setIdListPrice(Integer.parseInt(values[0]));
            key.setIdArticle(Integer.parseInt(values[1]));
            return key;
        }

        String getStringKey(Models.Entities.ListPricePK value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value.getIdListPrice());
            sb.append(SEPARATOR);
            sb.append(value.getIdArticle());
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof ListPrice) {
                ListPrice o = (ListPrice) object;
                return getStringKey(o.getListPricePK());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), ListPrice.class.getName()});
                return null;
            }
        }

    }

    /**
     * Método para dar memoria a las variables que serán utilizadas para guardar los registros.
     */
    public void inicializarValores() {
        newListPrice = new ListPrice(new ListPricePK(0, 0));
        listado = ejbFacade.cargarListado();
        tomorrow = new Date();
        today = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(tomorrow);
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        tomorrow = calendar.getTime(); //día de mañana
    }

    /**
     * Método para crear el registro de lista de precios, se guarda primeramente el inventario, luego el artículo y por último se le asigna un precio.
     */
    public void crearRegistro() {
        boolean band = true;
        String msj = "";
        java.util.Date fecha = new Date();

        //Fechas mayores al día actual (Por política de negocio), los precios no cambian durante el día.
        if (newListPrice.getDateini().before(fecha) || newListPrice.getDatefin().before(fecha)) {
            band = false;
            msj = "The final and initial date must be higher than today";
        }

        //Fecha final mayor o igual a fecha inicial.
        if (band && newListPrice.getDatefin().before(newListPrice.getDateini())) {
            band = false;
            msj = "Change Final Date, Must be greater than or equal to the Initial Date";
        }

        //Se consulta que la fecha inicial sea mayor a la última fecha inicial elegida para el articulo.
        ListPrice lastItemDate = ejbFacade.ultimoPrecioPorArticulo(newListPrice.getArticle().getIdArticle());
        if (band && lastItemDate != null) {
            if (!newListPrice.getDateini().after(lastItemDate.getDateini())) {
                band = false;
                DateFormat day = new SimpleDateFormat("dd/MM/YYYY");
                String dateConverter = day.format(lastItemDate.getDateini());
                msj = "Change Initial Date, It must be greater than the last date of initial listing price for this item, greater than " + dateConverter;
            }
        }

        if (band) {
            UserTransaction transaction = null;
            try {
                transaction = (UserTransaction) new InitialContext().lookup("java:comp/UserTransaction");
                transaction.begin();

                /**
                 * Si la fecha final del último articulo encontrado es mayor a la fecha inicial del nuevo precio, la fecha final del articulo encontrado se
                 * modifica y pasa a ser un día antes a la fecha del nuevo precio.
                 */
                if (lastItemDate != null) {
                    if (lastItemDate.getDatefin().after(newListPrice.getDateini())) {
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTime(newListPrice.getDateini());
                        calendar.add(Calendar.DAY_OF_YEAR, -1);
                        lastItemDate.setDatefin(calendar.getTime());
                        ejbFacade.edit(lastItemDate);
                    }
                }

                newListPrice.getListPricePK().setIdArticle(newListPrice.getArticle().getIdArticle());
                newListPrice.setUserCreate("arcarrero");
                newListPrice.setDateCreate(fecha);
                ejbFacade.create(newListPrice);

                transaction.commit();

                listado = ejbFacade.cargarListado();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, ResourceBundle.getBundle("/Bundle").getString("ListPriceCreated"), ""));
                newListPrice = new ListPrice(new ListPricePK(0, 0));
            } catch (Exception e) {
                if (transaction != null) {
                    try {
                        transaction.rollback();
                    } catch (Exception ex) {
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"), ""));
                    }
                }
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"), ""));
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, msj, ""));
        }
    }

    public ListPrice getNewListPrice() {
        return newListPrice;
    }

    public void setNewListPrice(ListPrice newListPrice) {
        this.newListPrice = newListPrice;
    }

    public ListPriceFacade getEjbFacade() {
        return ejbFacade;
    }

    public void setEjbFacade(ListPriceFacade ejbFacade) {
        this.ejbFacade = ejbFacade;
    }

    public ArticleController getArticleController() {
        return articleController;
    }

    public void setArticleController(ArticleController articleController) {
        this.articleController = articleController;
    }

    public List<ListPrice> getListado() {
        return listado;
    }

    public void setListado(List<ListPrice> listado) {
        this.listado = listado;
    }

    public Date getTomorrow() {
        return tomorrow;
    }

    public void setTomorrow(Date tomorrow) {
        this.tomorrow = tomorrow;
    }

    public Date getToday() {
        return today;
    }

    public void setToday(Date today) {
        this.today = today;
    }

}
