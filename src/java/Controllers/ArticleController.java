package Controllers;

import Models.Entities.Article;
import Controllers.util.JsfUtil;
import Controllers.util.JsfUtil.PersistAction;
import Models.Entities.ListPrice;
import Models.Entities.ListPricePK;
import Models.Queries.ArticleFacade;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.naming.InitialContext;
import javax.servlet.ServletContext;
import javax.transaction.UserTransaction;
import org.apache.commons.io.FileUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

@Named("articleController")
@SessionScoped
public class ArticleController implements Serializable {

    @EJB
    private Models.Queries.ArticleFacade ejbFacade;
    private List<Article> items = null;
    private Article selected;
    private List<Article> listado;
    private Article newArticle;
    private Date fecha_ini;
    private Date fecha_fin;
    private long precio;
    private UploadedFile file;
    private String urlFile;

    public ArticleController() {
    }

    public Article getSelected() {
        return selected;
    }

    public void setSelected(Article selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private ArticleFacade getFacade() {
        return ejbFacade;
    }

    public Article prepareCreate() {
        selected = new Article();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("ArticleCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("ArticleUpdated"));
        RequestContext confirm = RequestContext.getCurrentInstance();
        confirm.execute("PF('ArticleEditDialog').hide();");
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("ArticleDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Article> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    java.util.Date fecha = new Date();
                    selected.setDateEdit(fecha);
                    selected.setUserEdit("arcarrero");
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public Article getArticle(java.lang.Integer id) {
        return getFacade().find(id);
    }

    public List<Article> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Article> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Article.class)
    public static class ArticleControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            ArticleController controller = (ArticleController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "articleController");
            return controller.getArticle(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Article) {
                Article o = (Article) object;
                return getStringKey(o.getIdArticle());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Article.class.getName()});
                return null;
            }
        }

    }

    public ArticleFacade getEjbFacade() {
        return ejbFacade;
    }

    public void setEjbFacade(ArticleFacade ejbFacade) {
        this.ejbFacade = ejbFacade;
    }

    public Article getNewArticle() {
        return newArticle;
    }

    public void setNewArticle(Article newArticle) {
        this.newArticle = newArticle;
    }

    public Date getFecha_ini() {
        return fecha_ini;
    }

    public void setFecha_ini(Date fecha_ini) {
        this.fecha_ini = fecha_ini;
    }

    public Date getFecha_fin() {
        return fecha_fin;
    }

    public void setFecha_fin(Date fecha_fin) {
        this.fecha_fin = fecha_fin;
    }

    public long getPrecio() {
        return precio;
    }

    public void setPrecio(long precio) {
        this.precio = precio;
    }

    public List<Article> getListado() {
        return listado;
    }

    public void setListado(List<Article> listado) {
        this.listado = listado;
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    /**
     * Método para dar memoria a las variables que serán utilizadas para guardar los registros.
     */
    public void inicializarValores() {
        newArticle = new Article(0);
        listado = new ArrayList<>();
        listado = ejbFacade.findAll();
        urlFile = "";
    }

    /**
     * Método para crear el registro de lista de precios, se guarda primeramente el inventario, luego el artículo y por último se le asigna un precio.
     */
    public void crearRegistro() {
        UserTransaction transaction = null;

        if (urlFile.compareTo("") != 0) {
            try {
                transaction = (UserTransaction) new InitialContext().lookup("java:comp/UserTransaction");
                transaction.begin();
                java.util.Date fecha = new Date();

                newArticle.setUrlImage(urlFile);
                newArticle.setUserCreate("arcarrero");
                newArticle.setDateCreate(fecha);
                ejbFacade.create(newArticle);
                listado = ejbFacade.findAll();
                transaction.commit();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, ResourceBundle.getBundle("/Bundle").getString("ArticleCreated"), ""));
                inicializarValores();
            } catch (Exception e) {
                if (transaction != null) {
                    try {
                        transaction.rollback();
                    } catch (Exception ex) {
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"), ""));
                    }
                }
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"), ""));
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Image it's required!", ""));
        }
    }

    /**
     * Subida de imágenes
     *
     * @param event
     */
    public void handleFileUpload(FileUploadEvent event) throws IOException {
        InputStream inputStr = null;
        inputStr = event.getFile().getInputstream();

        FacesContext fcontext = FacesContext.getCurrentInstance();
        ServletContext context = (ServletContext) fcontext.getExternalContext().getContext();
        String destPath = context.getRealPath("").replace("\\build\\web", "") + "\\web\\resources\\img\\upload\\" + event.getFile().getFileName();
        urlFile = "../../resources/img/upload/" + event.getFile().getFileName();
        File destFile = new File(destPath);

        FileUtils.copyInputStreamToFile(inputStr, destFile);
    }

}
