package Controllers;

import Models.Entities.Orders;
import Controllers.util.JsfUtil;
import Controllers.util.JsfUtil.PersistAction;
import Models.Entities.Article;
import Models.Entities.Bill;
import Models.Entities.BillPK;
import Models.Entities.ListPrice;
import Models.Entities.OrdersPK;
import Models.Entities.PayMode;
import Models.Queries.ArticleFacade;
import Models.Queries.BillFacade;
import Models.Queries.ListPriceFacade;
import Models.Queries.OrdersFacade;
import Models.Queries.PayModeFacade;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.event.ValueChangeEvent;
import javax.naming.InitialContext;
import javax.transaction.UserTransaction;
import org.primefaces.context.RequestContext;

@Named("ordersController")
@SessionScoped
public class OrdersController implements Serializable {

    @EJB
    private Models.Queries.OrdersFacade ejbFacade;
    @EJB
    private Models.Queries.ListPriceFacade listPriceFacade;
    @EJB
    private Models.Queries.BillFacade billFacade;
    @EJB
    private Models.Queries.ArticleFacade articleFacade;
    @EJB
    private Models.Queries.PayModeFacade payModeFacade;
    private List<Orders> items = null;
    private Orders selected;
    private List<Ventas> ventas;
    private float total = 0;
    private Bill factura;
    private PayMode payMode;
    private String expDate;

    public OrdersController() {
    }

    public Orders getSelected() {
        return selected;
    }

    public void setSelected(Orders selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
        selected.getOrdersPK().setIdBill(selected.getBill().getBillPK().getIdBill());
        selected.getOrdersPK().setIdArticle(selected.getListPrice().getListPricePK().getIdArticle());
        selected.getOrdersPK().setIdUser(selected.getBill().getBillPK().getIdUser());
        selected.getOrdersPK().setIdListPrice(selected.getListPrice().getListPricePK().getIdListPrice());
    }

    protected void initializeEmbeddableKey() {
        selected.setOrdersPK(new Models.Entities.OrdersPK());
    }

    private OrdersFacade getFacade() {
        return ejbFacade;
    }

    public Orders prepareCreate() {
        selected = new Orders();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("OrdersCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("OrdersUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("OrdersDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Orders> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public Orders getOrders(Models.Entities.OrdersPK id) {
        return getFacade().find(id);
    }

    public List<Orders> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Orders> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Orders.class)
    public static class OrdersControllerConverter implements Converter {

        private static final String SEPARATOR = "#";
        private static final String SEPARATOR_ESCAPED = "\\#";

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            OrdersController controller = (OrdersController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "ordersController");
            return controller.getOrders(getKey(value));
        }

        Models.Entities.OrdersPK getKey(String value) {
            Models.Entities.OrdersPK key;
            String values[] = value.split(SEPARATOR_ESCAPED);
            key = new Models.Entities.OrdersPK();
            key.setIdOrder(Integer.parseInt(values[0]));
            key.setIdListPrice(Integer.parseInt(values[1]));
            key.setIdArticle(Integer.parseInt(values[2]));
            key.setIdBill(Integer.parseInt(values[3]));
            key.setIdUser(Integer.parseInt(values[4]));
            return key;
        }

        String getStringKey(Models.Entities.OrdersPK value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value.getIdOrder());
            sb.append(SEPARATOR);
            sb.append(value.getIdListPrice());
            sb.append(SEPARATOR);
            sb.append(value.getIdArticle());
            sb.append(SEPARATOR);
            sb.append(value.getIdBill());
            sb.append(SEPARATOR);
            sb.append(value.getIdUser());
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Orders) {
                Orders o = (Orders) object;
                return getStringKey(o.getOrdersPK());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Orders.class.getName()});
                return null;
            }
        }

    }

    public OrdersFacade getEjbFacade() {
        return ejbFacade;
    }

    public void setEjbFacade(OrdersFacade ejbFacade) {
        this.ejbFacade = ejbFacade;
    }

    public ListPriceFacade getListPriceFacade() {
        return listPriceFacade;
    }

    public void setListPriceFacade(ListPriceFacade listPriceFacade) {
        this.listPriceFacade = listPriceFacade;
    }

    public List<Ventas> getVentas() {
        return ventas;
    }

    public void setVentas(List<Ventas> ventas) {
        this.ventas = ventas;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public BillFacade getBillFacade() {
        return billFacade;
    }

    public void setBillFacade(BillFacade billFacade) {
        this.billFacade = billFacade;
    }

    public ArticleFacade getArticleFacade() {
        return articleFacade;
    }

    public void setArticleFacade(ArticleFacade articleFacade) {
        this.articleFacade = articleFacade;
    }

    public Bill getFactura() {
        return factura;
    }

    public void setFactura(Bill factura) {
        this.factura = factura;
    }

    public PayMode getPayMode() {
        return payMode;
    }

    public void setPayMode(PayMode payMode) {
        this.payMode = payMode;
    }

    public PayModeFacade getPayModeFacade() {
        return payModeFacade;
    }

    public void setPayModeFacade(PayModeFacade payModeFacade) {
        this.payModeFacade = payModeFacade;
    }

    public String getExpDate() {
        return expDate;
    }

    public void setExpDate(String expDate) {
        this.expDate = expDate;
    }
    
    

    /**
     * Método para dar memoria a las variables que serán utilizadas para guardar los registros.
     */
    public void inicializarValores() {
        payMode = new PayMode();
        total = 0;
        ventas = new ArrayList<>();
        Date today = new Date();
        List<ListPrice> listado = listPriceFacade.cargarListadoVigente(today);
        if (listado != null) {
            for (ListPrice listado1 : listado) {
                Ventas vendiendo = new Ventas();
                vendiendo.setIdArticle(listado1.getArticle().getIdArticle());
                vendiendo.setIdListPrice(listado1.getListPricePK().getIdListPrice());
                vendiendo.setNameArticle(listado1.getArticle().getName());
                vendiendo.setPrice(listado1.getPrice());
                vendiendo.setOrder(0);
                vendiendo.setCantMax(listado1.getArticle().getStock());
                vendiendo.setUrlImage(listado1.getArticle().getUrlImage());
                ventas.add(vendiendo);
            }
        }
    }

    /**
     * Método para calcular el total de la compra
     */
    public void refreshTotal() {
        total = 0;
        for (Ventas item : ventas) {
            total += item.getOrder() * item.getPrice();
        }
    }

    /**
     * Método que guarda la orden realizada.
     *
     * @param paymode true: realizó pago, false: solo realizó la orden.
     */
    public void crearOrden(boolean paymode) {

        Date today = new Date();
        UserTransaction transaction = null;
        try {
            transaction = (UserTransaction) new InitialContext().lookup("java:comp/UserTransaction");
            transaction.begin();

            factura = new Bill(new BillPK());

            if (paymode) {
                this.payMode.setIdPayMode(0);
                payModeFacade.create(payMode);
                factura.setIdPayMode(payMode);
            }

            factura.getBillPK().setIdBill(0);
            factura.getBillPK().setIdUser(1);
            factura.setDateBill(today);
            billFacade.create(factura);

            //Consultar factura para obtener el valor autoincremental.
            factura = billFacade.ultimaFacturaUsuario(factura.getBillPK().getIdUser(), today);

            for (Ventas item : ventas) {
                if (item.getOrder() > 0) {
                    Orders orden = new Orders(new OrdersPK());
                    orden.getOrdersPK().setIdArticle(item.getIdArticle());
                    orden.getOrdersPK().setIdListPrice(item.getIdListPrice());
                    orden.getOrdersPK().setIdUser(factura.getBillPK().getIdUser());
                    orden.getOrdersPK();
                    orden.getOrdersPK().setIdOrder(0);
                    orden.setQuantity(item.getOrder());
                    orden.getOrdersPK().setIdBill(factura.getBillPK().getIdBill());
                    orden.setBill(factura);
                    ejbFacade.create(orden);

                    //Consultar artículo y disminuir Stock
                    Article articulo = articleFacade.find(item.getIdArticle());
                    articulo.setStock(articulo.getStock() - item.getOrder());
                    articleFacade.edit(articulo);
                }
            }

            transaction.commit();
            inicializarValores();

            RequestContext confirm = RequestContext.getCurrentInstance();
            confirm.execute("PF('dlgorderPay').hide();");

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Order Made", ""));
        } catch (Exception e) {
            if (transaction != null) {
                try {
                    transaction.rollback();
                } catch (Exception ex) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"), ""));
                }
            }
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"), ""));
        }
    }

}
