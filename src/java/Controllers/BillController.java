package Controllers;

import Models.Entities.Bill;
import Controllers.util.JsfUtil;
import Controllers.util.JsfUtil.PersistAction;
import Models.Entities.Article;
import Models.Entities.ListPrice;
import Models.Entities.Orders;
import Models.Entities.OrdersPK;
import Models.Queries.BillFacade;
import Models.Queries.ClientFacade;
import Models.Queries.ListPriceFacade;
import Models.Queries.OrdersFacade;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.naming.InitialContext;
import javax.transaction.UserTransaction;
import org.primefaces.context.RequestContext;

@Named("billController")
@SessionScoped
public class BillController implements Serializable {

    @EJB
    private Models.Queries.BillFacade ejbFacade;
    @EJB
    private Models.Queries.ClientFacade clientFacade;
    @EJB
    private Models.Queries.OrdersFacade orderFacade;
    @EJB
    private Models.Queries.ListPriceFacade listPriceFacade;
    @EJB
    private Models.Queries.ArticleFacade articleFacade;
    private List<Bill> items = null;
    private Bill selected;
    private List<Orders> detallePedido;
    private List<Ventas> ventas;
    private List<Ventas> ventasAnterior;
    private float total;
    private boolean editable;

    public BillController() {
    }

    public Bill getSelected() {
        return selected;
    }

    public void setSelected(Bill selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
        selected.getBillPK().setIdUser(selected.getClient().getIdUser());
    }

    protected void initializeEmbeddableKey() {
        selected.setBillPK(new Models.Entities.BillPK());
    }

    private BillFacade getFacade() {
        return ejbFacade;
    }

    public Bill prepareCreate() {
        selected = new Bill();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("BillCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("BillUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("BillDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Bill> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public Bill getBill(Models.Entities.BillPK id) {
        return getFacade().find(id);
    }

    public List<Bill> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Bill> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    public BillFacade getEjbFacade() {
        return ejbFacade;
    }

    public void setEjbFacade(BillFacade ejbFacade) {
        this.ejbFacade = ejbFacade;
    }

    public ClientFacade getClientFacade() {
        return clientFacade;
    }

    public void setClientFacade(ClientFacade clientFacade) {
        this.clientFacade = clientFacade;
    }

    public OrdersFacade getOrderFacade() {
        return orderFacade;
    }

    public void setOrderFacade(OrdersFacade orderFacade) {
        this.orderFacade = orderFacade;
    }

    public List<Orders> getDetallePedido() {
        return detallePedido;
    }

    public void setDetallePedido(List<Orders> detallePedido) {
        this.detallePedido = detallePedido;
    }

    @FacesConverter(forClass = Bill.class)
    public static class BillControllerConverter implements Converter {

        private static final String SEPARATOR = "#";
        private static final String SEPARATOR_ESCAPED = "\\#";

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            BillController controller = (BillController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "billController");
            return controller.getBill(getKey(value));
        }

        Models.Entities.BillPK getKey(String value) {
            Models.Entities.BillPK key;
            String values[] = value.split(SEPARATOR_ESCAPED);
            key = new Models.Entities.BillPK();
            key.setIdBill(Integer.parseInt(values[0]));
            key.setIdUser(Integer.parseInt(values[1]));
            return key;
        }

        String getStringKey(Models.Entities.BillPK value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value.getIdBill());
            sb.append(SEPARATOR);
            sb.append(value.getIdUser());
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Bill) {
                Bill o = (Bill) object;
                return getStringKey(o.getBillPK());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Bill.class.getName()});
                return null;
            }
        }

    }

    public ListPriceFacade getListPriceFacade() {
        return listPriceFacade;
    }

    public void setListPriceFacade(ListPriceFacade listPriceFacade) {
        this.listPriceFacade = listPriceFacade;
    }

    public List<Ventas> getVentas() {
        return ventas;
    }

    public void setVentas(List<Ventas> ventas) {
        this.ventas = ventas;
    }

    public List<Ventas> getVentasAnterior() {
        return ventasAnterior;
    }

    public void setVentasAnterior(List<Ventas> ventasAnterior) {
        this.ventasAnterior = ventasAnterior;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    /**
     * Método para inicializar variables y cargar valores en la página.
     */
    public void inicializarValores() {
        ventas = new ArrayList<>();
        ventasAnterior = new ArrayList<>();
        detallePedido = new ArrayList<>();
        items = ejbFacade.buscarFacturas();
        total = 0;
        editable = false;
    }

    /**
     * Buscar cliente según su código
     *
     * @param _idUser código del usuario
     * @return Nombre del usuario.
     */
    public String buscarClienteById(Integer _idUser) {
        return clientFacade.find(_idUser).getName();
    }

    /**
     * Método para calcular el total de la factura.
     *
     * @param _item Factura
     * @return Total de la factura
     */
    public float calcularTotal(Bill _item) {

        float total = 0;
        List<Orders> pedido = orderFacade.buscarPedido(_item.getBillPK().getIdBill(), _item.getBillPK().getIdUser());

        for (Orders pedidoDetalle : pedido) {
            total += pedidoDetalle.getListPrice().getPrice() * pedidoDetalle.getQuantity();
        }

        return total;
    }

    /**
     * Método para cargar el detalle del pedido que realizó el cliente.
     */
    public void observarDetalle() {
        total = 0;
        ventas = new ArrayList<>();
        ventasAnterior = new ArrayList<>();
        detallePedido = orderFacade.buscarPedido(selected.getBillPK().getIdBill(), selected.getBillPK().getIdUser());

        List<ListPrice> listado = listPriceFacade.cargarListadoVigente(selected.getDateBill());
        if (listado != null) {
            for (ListPrice listado1 : listado) {
                boolean band = false;
                Ventas vendiendo = new Ventas();
                vendiendo.setIdArticle(listado1.getArticle().getIdArticle());
                vendiendo.setIdListPrice(listado1.getListPricePK().getIdListPrice());
                vendiendo.setNameArticle(listado1.getArticle().getName());
                vendiendo.setPrice(listado1.getPrice());
                for (int i = 0; i < detallePedido.size(); i++) {
                    if (detallePedido.get(i).getListPrice().getListPricePK().getIdArticle() == listado1.getListPricePK().getIdArticle()
                            && detallePedido.get(i).getListPrice().getListPricePK().getIdListPrice() == listado1.getListPricePK().getIdListPrice()) {
                        band = true;
                        vendiendo.setOrder(detallePedido.get(i).getQuantity());
                        total += detallePedido.get(i).getQuantity() * detallePedido.get(i).getListPrice().getPrice();
                    }
                    vendiendo.setIdBill(detallePedido.get(i).getOrdersPK().getIdBill());
                    editable = detallePedido.get(i).getBill().getIdPayMode() != null;
                }
                if (!band) {
                    vendiendo.setOrder(0);
                }
                vendiendo.setCantMax(listado1.getArticle().getStock());
                vendiendo.setUrlImage(listado1.getArticle().getUrlImage());
                ventas.add(vendiendo);
            }
        }

        //Clonando el obj
        for (Ventas ventasItem : ventas) {
            Ventas vendiendo = new Ventas();
            if (ventasItem.getOrder() > 0) {
                vendiendo.setIdArticle(ventasItem.getIdArticle());
                vendiendo.setIdListPrice(ventasItem.getIdListPrice());
                vendiendo.setNameArticle(ventasItem.getNameArticle());
                vendiendo.setPrice(ventasItem.getPrice());
                vendiendo.setOrder(ventasItem.getOrder());
                vendiendo.setIdBill(ventasItem.getIdBill());
                vendiendo.setCantMax(ventasItem.getCantMax());
                vendiendo.setUrlImage(ventasItem.getUrlImage());
                ventasAnterior.add(vendiendo);
            }
        }

        RequestContext confirm = RequestContext.getCurrentInstance();
        confirm.execute("PF('dlgBillDetail').show();");

    }

    /**
     * Método para calcular el total de la compra
     */
    public void refreshTotal() {
        total = 0;
        for (Ventas item : ventas) {
            total += item.getOrder() * item.getPrice();
        }
    }

    /**
     * Método para realizar edición del pedido por el administrador.
     */
    public void realizarEdicion() {

        UserTransaction transaction = null;
        try {
            transaction = (UserTransaction) new InitialContext().lookup("java:comp/UserTransaction");
            transaction.begin();

            for (Ventas ventaFinal : ventas) {
                boolean band = false;
                for (Ventas ventaInicial : ventasAnterior) {
                    if (ventaInicial.getIdArticle() == ventaFinal.getIdArticle()
                            && ventaInicial.getIdListPrice() == ventaFinal.getIdListPrice()
                            && ventaInicial.getOrder() != ventaFinal.getOrder()) {
                        /**
                         * Se encontró en la venta inicial el mismo artículo pero con diferente cantidad solicitada, debemos actualizar el registro, se
                         * hizo una edición del pedido.
                         */
                        band = true;
                        Orders editOrder = orderFacade.buscarPedidoDetallado(ventaInicial.getIdBill(), ventaInicial.getIdArticle(), ventaInicial.getIdListPrice());
                        editOrder.setQuantity(ventaFinal.getOrder());
                        orderFacade.edit(editOrder);

                        //Cambiando la cantidad de Stock.
                        Article articulo = articleFacade.find(editOrder.getOrdersPK().getIdArticle());
                        int stock = ventaInicial.getOrder() - ventaFinal.getOrder();
                        articulo.setStock(stock + articulo.getStock());
                        articleFacade.edit(articulo);
                    }
                }

                if (!band) {
                    /**
                     * Si band sigue siendo falso, es que se está incluyendo un artículo nuevo al pedido, se debe crear el registro y disminuir el Stock.
                     */
                    Orders createOrder = new Orders(new OrdersPK());
                    createOrder.getOrdersPK().setIdArticle(ventaFinal.getIdArticle());
                    createOrder.getOrdersPK().setIdBill(ventaFinal.getIdBill());
                    createOrder.getOrdersPK().setIdListPrice(ventaFinal.getIdListPrice());
                    createOrder.getOrdersPK().setIdUser(1);
                    createOrder.getOrdersPK().setIdOrder(0);

                    createOrder.setQuantity(ventaFinal.getOrder());
                    orderFacade.create(createOrder);

                    //Disminuyo Stock.
                    Article articulo = articleFacade.find(createOrder.getOrdersPK().getIdArticle());
                    articulo.setStock(articulo.getStock() - ventaFinal.getOrder());
                    articleFacade.edit(articulo);
                }
            }

            transaction.commit();
            inicializarValores();

            RequestContext confirm = RequestContext.getCurrentInstance();
            confirm.execute("PF('dlgBillDetail').hide();");

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Order Made", ""));
        } catch (Exception e) {
            if (transaction != null) {
                try {
                    transaction.rollback();
                } catch (Exception ex) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"), ""));
                }
            }
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"), ""));
        }
    }
}
