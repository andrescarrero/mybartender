/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.Queries;

import Models.Entities.Orders;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author ARCarrero
 */
@Stateless
public class OrdersFacade extends AbstractFacade<Orders> {

    @PersistenceContext(unitName = "MyBartenderPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public OrdersFacade() {
        super(Orders.class);
    }

    /**
     * Método para consultar el pedido realizado por el usuario, según su número de factura.
     *
     * @param idBill Id. Factura
     * @param idUser Id. Usuario
     * @return Pedido
     */
    public List<Orders> buscarPedido(int idBill, int idUser) {
        try {

            List<Orders> listado = getEntityManager()
                    .createQuery("SELECT t FROM Orders t WHERE t.ordersPK.idBill=:idBill and t.ordersPK.idUser=:idUser")
                    .setParameter("idBill", idBill)
                    .setParameter("idUser", idUser)
                    .getResultList();

            return listado;
        } catch (Exception e) {
            System.out.println("e: " + e.getMessage());
            return null;
        }
    }

    /**
     * Método para buscar el detalle de un pedido.
     *
     * @param idBill id de la factura
     * @param idArticle id del articulo
     * @param idListPrice id de la lista de precio
     * @return Order realizada.
     */
    public Orders buscarPedidoDetallado(int idBill, int idArticle, int idListPrice) {
        try {

            Orders registro = (Orders) getEntityManager()
                    .createQuery("SELECT t FROM Orders t WHERE t.ordersPK.idBill=:idBill and t.ordersPK.idArticle=:idArticle and t.ordersPK.idListPrice=:idListPrice")
                    .setParameter("idBill", idBill)
                    .setParameter("idArticle", idArticle)
                    .setParameter("idListPrice", idListPrice)
                    .getSingleResult();

            return registro;
        } catch (Exception e) {
            System.out.println("e: " + e.getMessage());
            return null;
        }
    }

}
