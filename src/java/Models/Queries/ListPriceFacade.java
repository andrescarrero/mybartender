/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.Queries;

import Models.Entities.ListPrice;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;

/**
 *
 * @author ARCarrero
 */
@Stateless
public class ListPriceFacade extends AbstractFacade<ListPrice> {

    @PersistenceContext(unitName = "MyBartenderPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ListPriceFacade() {
        super(ListPrice.class);
    }

    /**
     * Se consultan todos los precios de un artículo, se ordena por fecha inicial descendentemente y se escoje el primero
     *
     * @param idArticle id del artículo
     * @return ListPrice
     */
    public ListPrice ultimoPrecioPorArticulo(Integer idArticle) {
        try {
            String sql = "SELECT t FROM ListPrice t WHERE t.listPricePK.idArticle=" + idArticle + " ORDER BY t.dateini DESC";
            return (ListPrice) getEntityManager().createQuery(sql).getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Se consulta si se solapa la fecha inicial con algúna lista de precio para ese artículo.
     *
     * @param listPrice obj listado de precio seleccionado
     * @param date fecha
     * @return ListPrice
     */
    public ListPrice buscarListaPrecioSolapada(ListPrice listPrice, Date date) {
        try {
            String sql = "SELECT t FROM ListPrice t WHERE t.listPricePK.idArticle=:idArticle and t.listPricePK.idListPrice!=:idListPrice and  :dateIni BETWEEN t.dateini and t.datefin ORDER BY t.dateini DESC";
            return (ListPrice) getEntityManager().createQuery(sql)
                    .setParameter("idArticle", listPrice.getListPricePK().getIdArticle())
                    .setParameter("idListPrice", listPrice.getListPricePK().getIdListPrice())
                    .setParameter("dateIni", date, TemporalType.DATE)
                    .getSingleResult();
        } catch (Exception e) {
            System.out.println("e: " + e.getMessage());
            return null;
        }
    }

    /**
     * Cargar el listado de precios ordenados por producto y fecha.
     *
     * @return Listado existente.
     */
    public List<ListPrice> cargarListado() {
        try {

            List<ListPrice> listado = getEntityManager().createQuery("SELECT t FROM ListPrice t ORDER BY t.article.name, t.dateini DESC")
                    .getResultList();

            return listado;
        } catch (Exception e) {
            System.out.println("e: " + e.getMessage());
            return null;
        }
    }

    /**
     * Método para cargar el listado de los artículos que están a la venta.
     *
     * @param today día actual
     * @return Listado de articulos con precios.
     */
    public List<ListPrice> cargarListadoVigente(Date today) {
        try {

            List<ListPrice> listado = getEntityManager()
                    .createQuery("SELECT t FROM ListPrice t WHERE t.article.state='Enable' and t.article.stock>0 and :today BETWEEN t.dateini and t.datefin ORDER BY t.article.name")
                    .setParameter("today", today, TemporalType.DATE)
                    .getResultList();

            return listado;
        } catch (Exception e) {
            System.out.println("e: " + e.getMessage());
            return null;
        }
    }

}
