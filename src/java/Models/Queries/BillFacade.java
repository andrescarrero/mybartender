/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.Queries;

import Models.Entities.Bill;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;

/**
 *
 * @author ARCarrero
 */
@Stateless
public class BillFacade extends AbstractFacade<Bill> {

    @PersistenceContext(unitName = "MyBartenderPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public BillFacade() {
        super(Bill.class);
    }

    /**
     * Método para consultar una factura de un usuario en determinada fecha
     *
     * @param idUser id del usuario
     * @param fecha fecha para la consulta
     * @return Factura encontrada.
     */
    public Bill ultimaFacturaUsuario(int idUser, Date fecha) {
        try {

            Bill factura = (Bill) getEntityManager()
                    .createQuery("SELECT t FROM Bill t WHERE t.billPK.idUser=:idUser and t.dateBill=:fecha")
                    .setParameter("idUser", idUser)
                    .setParameter("fecha", fecha, TemporalType.TIMESTAMP)
                    .getSingleResult();

            return factura;
        } catch (Exception e) {
            System.out.println("e: " + e.getMessage());
            return null;
        }
    }

    /**
     * Método para buscar todas las facturas existentes ordenadas por número de factura, cliente y fecha.
     *
     * @return Listado de facturas encontradas
     */
    public List<Bill> buscarFacturas() {
        try {

            List<Bill> factura = getEntityManager()
                    .createQuery("SELECT t FROM Bill t ORDER BY t.billPK.idUser , t.billPK.idBill, t.dateBill desc")
                    .getResultList();

            return factura;
        } catch (Exception e) {
            System.out.println("e: " + e.getMessage());
            return null;
        }
    }

}
