/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.Queries;

import Models.Entities.PayMode;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author ARCarrero
 */
@Stateless
public class PayModeFacade extends AbstractFacade<PayMode> {
    @PersistenceContext(unitName = "MyBartenderPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PayModeFacade() {
        super(PayMode.class);
    }
    
}
