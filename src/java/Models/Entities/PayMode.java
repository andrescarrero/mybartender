/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.Entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ARCarrero
 */
@Entity
@Table(name = "pay_mode")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PayMode.findAll", query = "SELECT p FROM PayMode p"),
    @NamedQuery(name = "PayMode.findByIdPayMode", query = "SELECT p FROM PayMode p WHERE p.idPayMode = :idPayMode"),
    @NamedQuery(name = "PayMode.findByCardType", query = "SELECT p FROM PayMode p WHERE p.cardType = :cardType"),
    @NamedQuery(name = "PayMode.findByCardNumber", query = "SELECT p FROM PayMode p WHERE p.cardNumber = :cardNumber"),
    @NamedQuery(name = "PayMode.findBySecurityCode", query = "SELECT p FROM PayMode p WHERE p.securityCode = :securityCode"),
    @NamedQuery(name = "PayMode.findByNameCard", query = "SELECT p FROM PayMode p WHERE p.nameCard = :nameCard"),
    @NamedQuery(name = "PayMode.findByIdentificationCard", query = "SELECT p FROM PayMode p WHERE p.identificationCard = :identificationCard")})
public class PayMode implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idPay_Mode")
    private Integer idPayMode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "card_type")
    private String cardType;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 12)
    @Column(name = "card_number")
    private String cardNumber;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "security_code")
    private String securityCode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "name_card")
    private String nameCard;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "identification_card")
    private String identificationCard;
    @OneToMany(mappedBy = "idPayMode", fetch = FetchType.LAZY)
    private List<Bill> billList;

    public PayMode() {
    }

    public PayMode(Integer idPayMode) {
        this.idPayMode = idPayMode;
    }

    public PayMode(Integer idPayMode, String cardType, String cardNumber, String securityCode, String nameCard, String identificationCard) {
        this.idPayMode = idPayMode;
        this.cardType = cardType;
        this.cardNumber = cardNumber;
        this.securityCode = securityCode;
        this.nameCard = nameCard;
        this.identificationCard = identificationCard;
    }

    public Integer getIdPayMode() {
        return idPayMode;
    }

    public void setIdPayMode(Integer idPayMode) {
        this.idPayMode = idPayMode;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getSecurityCode() {
        return securityCode;
    }

    public void setSecurityCode(String securityCode) {
        this.securityCode = securityCode;
    }

    public String getNameCard() {
        return nameCard;
    }

    public void setNameCard(String nameCard) {
        this.nameCard = nameCard;
    }

    public String getIdentificationCard() {
        return identificationCard;
    }

    public void setIdentificationCard(String identificationCard) {
        this.identificationCard = identificationCard;
    }

    @XmlTransient
    public List<Bill> getBillList() {
        return billList;
    }

    public void setBillList(List<Bill> billList) {
        this.billList = billList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPayMode != null ? idPayMode.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PayMode)) {
            return false;
        }
        PayMode other = (PayMode) object;
        if ((this.idPayMode == null && other.idPayMode != null) || (this.idPayMode != null && !this.idPayMode.equals(other.idPayMode))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Models.Entities.PayMode[ idPayMode=" + idPayMode + " ]";
    }
    
}
