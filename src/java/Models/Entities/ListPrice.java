/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.Entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ARCarrero
 */
@Entity
@Table(name = "list_price")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ListPrice.findAll", query = "SELECT l FROM ListPrice l"),
    @NamedQuery(name = "ListPrice.findByIdListPrice", query = "SELECT l FROM ListPrice l WHERE l.listPricePK.idListPrice = :idListPrice"),
    @NamedQuery(name = "ListPrice.findByPrice", query = "SELECT l FROM ListPrice l WHERE l.price = :price"),
    @NamedQuery(name = "ListPrice.findByDateini", query = "SELECT l FROM ListPrice l WHERE l.dateini = :dateini"),
    @NamedQuery(name = "ListPrice.findByDatefin", query = "SELECT l FROM ListPrice l WHERE l.datefin = :datefin"),
    @NamedQuery(name = "ListPrice.findByUserCreate", query = "SELECT l FROM ListPrice l WHERE l.userCreate = :userCreate"),
    @NamedQuery(name = "ListPrice.findByDateCreate", query = "SELECT l FROM ListPrice l WHERE l.dateCreate = :dateCreate"),
    @NamedQuery(name = "ListPrice.findByUserEdit", query = "SELECT l FROM ListPrice l WHERE l.userEdit = :userEdit"),
    @NamedQuery(name = "ListPrice.findByDateEdit", query = "SELECT l FROM ListPrice l WHERE l.dateEdit = :dateEdit"),
    @NamedQuery(name = "ListPrice.findByIdArticle", query = "SELECT l FROM ListPrice l WHERE l.listPricePK.idArticle = :idArticle")})
public class ListPrice implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false)
    private float price;
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ListPricePK listPricePK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "dateini", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date dateini;
    @Basic(optional = false)
    @NotNull
    @Column(name = "datefin", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date datefin;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "user_create", nullable = false, length = 45)
    private String userCreate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date_create", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreate;
    @Size(max = 45)
    @Column(name = "user_edit", length = 45)
    private String userEdit;
    @Column(name = "date_edit")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateEdit;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "listPrice", fetch = FetchType.LAZY)
    private List<Orders> ordersList;
    @JoinColumn(name = "idArticle", referencedColumnName = "idArticle", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Article article;

    public ListPrice() {
    }

    public ListPrice(ListPricePK listPricePK) {
        this.listPricePK = listPricePK;
    }

    public ListPrice(ListPricePK listPricePK, long price, Date dateini, Date datefin, String userCreate, Date dateCreate) {
        this.listPricePK = listPricePK;
        this.price = price;
        this.dateini = dateini;
        this.datefin = datefin;
        this.userCreate = userCreate;
        this.dateCreate = dateCreate;
    }

    public ListPrice(int idListPrice, int idArticle) {
        this.listPricePK = new ListPricePK(idListPrice, idArticle);
    }

    public ListPricePK getListPricePK() {
        return listPricePK;
    }

    public void setListPricePK(ListPricePK listPricePK) {
        this.listPricePK = listPricePK;
    }


    public Date getDateini() {
        return dateini;
    }

    public void setDateini(Date dateini) {
        this.dateini = dateini;
    }

    public Date getDatefin() {
        return datefin;
    }

    public void setDatefin(Date datefin) {
        this.datefin = datefin;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public String getUserEdit() {
        return userEdit;
    }

    public void setUserEdit(String userEdit) {
        this.userEdit = userEdit;
    }

    public Date getDateEdit() {
        return dateEdit;
    }

    public void setDateEdit(Date dateEdit) {
        this.dateEdit = dateEdit;
    }

    @XmlTransient
    public List<Orders> getOrdersList() {
        return ordersList;
    }

    public void setOrdersList(List<Orders> ordersList) {
        this.ordersList = ordersList;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (listPricePK != null ? listPricePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ListPrice)) {
            return false;
        }
        ListPrice other = (ListPrice) object;
        if ((this.listPricePK == null && other.listPricePK != null) || (this.listPricePK != null && !this.listPricePK.equals(other.listPricePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Models.Entities.ListPrice[ listPricePK=" + listPricePK + " ]";
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
    
}
