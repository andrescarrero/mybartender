/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.Entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author ARCarrero
 */
@Embeddable
public class BillPK implements Serializable {
    @Basic(optional = false)
    private int idBill;
    @Basic(optional = false)
    @NotNull
    private int idUser;

    public BillPK() {
    }

    public BillPK(int idBill, int idUser) {
        this.idBill = idBill;
        this.idUser = idUser;
    }

    public int getIdBill() {
        return idBill;
    }

    public void setIdBill(int idBill) {
        this.idBill = idBill;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idBill;
        hash += (int) idUser;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BillPK)) {
            return false;
        }
        BillPK other = (BillPK) object;
        if (this.idBill != other.idBill) {
            return false;
        }
        if (this.idUser != other.idUser) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Models.Entities.BillPK[ idBill=" + idBill + ", idUser=" + idUser + " ]";
    }
    
}
