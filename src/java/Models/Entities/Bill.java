/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.Entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ARCarrero
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Bill.findAll", query = "SELECT b FROM Bill b"),
    @NamedQuery(name = "Bill.findByIdBill", query = "SELECT b FROM Bill b WHERE b.billPK.idBill = :idBill"),
    @NamedQuery(name = "Bill.findByDateBill", query = "SELECT b FROM Bill b WHERE b.dateBill = :dateBill"),
    @NamedQuery(name = "Bill.findByIdUser", query = "SELECT b FROM Bill b WHERE b.billPK.idUser = :idUser")})
public class Bill implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected BillPK billPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date_bill")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateBill;
    @JoinColumn(name = "idUser", referencedColumnName = "idUser", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Client client;
    @JoinColumn(name = "idPay_Mode", referencedColumnName = "idPay_Mode")
    @ManyToOne(fetch = FetchType.LAZY)
    private PayMode idPayMode;

    public Bill() {
    }

    public Bill(BillPK billPK) {
        this.billPK = billPK;
    }

    public Bill(BillPK billPK, Date dateBill) {
        this.billPK = billPK;
        this.dateBill = dateBill;
    }

    public Bill(int idBill, int idUser) {
        this.billPK = new BillPK(idBill, idUser);
    }

    public BillPK getBillPK() {
        return billPK;
    }

    public void setBillPK(BillPK billPK) {
        this.billPK = billPK;
    }

    public Date getDateBill() {
        return dateBill;
    }

    public void setDateBill(Date dateBill) {
        this.dateBill = dateBill;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public PayMode getIdPayMode() {
        return idPayMode;
    }

    public void setIdPayMode(PayMode idPayMode) {
        this.idPayMode = idPayMode;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (billPK != null ? billPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Bill)) {
            return false;
        }
        Bill other = (Bill) object;
        if ((this.billPK == null && other.billPK != null) || (this.billPK != null && !this.billPK.equals(other.billPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Models.Entities.Bill[ billPK=" + billPK + " ]";
    }
    
}
