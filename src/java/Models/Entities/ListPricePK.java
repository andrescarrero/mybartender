/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.Entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author ARCarrero
 */
@Embeddable
public class ListPricePK implements Serializable {
    @Basic(optional = false)
    @Column(name = "idListPrice", nullable = false)
    private int idListPrice;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idArticle", nullable = false)
    private int idArticle;

    public ListPricePK() {
    }

    public ListPricePK(int idListPrice, int idArticle) {
        this.idListPrice = idListPrice;
        this.idArticle = idArticle;
    }

    public int getIdListPrice() {
        return idListPrice;
    }

    public void setIdListPrice(int idListPrice) {
        this.idListPrice = idListPrice;
    }

    public int getIdArticle() {
        return idArticle;
    }

    public void setIdArticle(int idArticle) {
        this.idArticle = idArticle;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idListPrice;
        hash += (int) idArticle;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ListPricePK)) {
            return false;
        }
        ListPricePK other = (ListPricePK) object;
        if (this.idListPrice != other.idListPrice) {
            return false;
        }
        if (this.idArticle != other.idArticle) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Models.Entities.ListPricePK[ idListPrice=" + idListPrice + ", idArticle=" + idArticle + " ]";
    }
    
}
