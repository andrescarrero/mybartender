/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.Entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ARCarrero
 */
@Entity
@Table(name = "orders")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Orders.findAll", query = "SELECT o FROM Orders o"),
    @NamedQuery(name = "Orders.findByIdOrder", query = "SELECT o FROM Orders o WHERE o.ordersPK.idOrder = :idOrder"),
    @NamedQuery(name = "Orders.findByQuantity", query = "SELECT o FROM Orders o WHERE o.quantity = :quantity"),
    @NamedQuery(name = "Orders.findByIdListPrice", query = "SELECT o FROM Orders o WHERE o.ordersPK.idListPrice = :idListPrice"),
    @NamedQuery(name = "Orders.findByIdArticle", query = "SELECT o FROM Orders o WHERE o.ordersPK.idArticle = :idArticle"),
    @NamedQuery(name = "Orders.findByIdBill", query = "SELECT o FROM Orders o WHERE o.ordersPK.idBill = :idBill"),
    @NamedQuery(name = "Orders.findByIdUser", query = "SELECT o FROM Orders o WHERE o.ordersPK.idUser = :idUser")})
public class Orders implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected OrdersPK ordersPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "quantity", nullable = false)
    private int quantity;
    @JoinColumns({
        @JoinColumn(name = "idBill", referencedColumnName = "idBill", nullable = false, insertable = false, updatable = false),
        @JoinColumn(name = "idUser", referencedColumnName = "idUser", nullable = false, insertable = false, updatable = false)})
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Bill bill;
    @JoinColumns({
        @JoinColumn(name = "idListPrice", referencedColumnName = "idListPrice", nullable = false, insertable = false, updatable = false),
        @JoinColumn(name = "idArticle", referencedColumnName = "idArticle", nullable = false, insertable = false, updatable = false)})
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ListPrice listPrice;

    public Orders() {
    }

    public Orders(OrdersPK ordersPK) {
        this.ordersPK = ordersPK;
    }

    public Orders(OrdersPK ordersPK, int quantity) {
        this.ordersPK = ordersPK;
        this.quantity = quantity;
    }

    public Orders(int idOrder, int idListPrice, int idArticle, int idBill, int idUser) {
        this.ordersPK = new OrdersPK(idOrder, idListPrice, idArticle, idBill, idUser);
    }

    public OrdersPK getOrdersPK() {
        return ordersPK;
    }

    public void setOrdersPK(OrdersPK ordersPK) {
        this.ordersPK = ordersPK;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Bill getBill() {
        return bill;
    }

    public void setBill(Bill bill) {
        this.bill = bill;
    }

    public ListPrice getListPrice() {
        return listPrice;
    }

    public void setListPrice(ListPrice listPrice) {
        this.listPrice = listPrice;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ordersPK != null ? ordersPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Orders)) {
            return false;
        }
        Orders other = (Orders) object;
        if ((this.ordersPK == null && other.ordersPK != null) || (this.ordersPK != null && !this.ordersPK.equals(other.ordersPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Models.Entities.Orders[ ordersPK=" + ordersPK + " ]";
    }
    
}
