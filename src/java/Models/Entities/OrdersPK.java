/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models.Entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author ARCarrero
 */
@Embeddable
public class OrdersPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "idOrder", nullable = false)
    private int idOrder;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idListPrice", nullable = false)
    private int idListPrice;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idArticle", nullable = false)
    private int idArticle;
    @Basic(optional = false)
    @Column(name = "idBill")
    private int idBill;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idUser", nullable = false)
    private int idUser;

    public OrdersPK() {
    }

    public OrdersPK(int idOrder, int idListPrice, int idArticle, int idBill, int idUser) {
        this.idOrder = idOrder;
        this.idListPrice = idListPrice;
        this.idArticle = idArticle;
        this.idBill = idBill;
        this.idUser = idUser;
    }

    public int getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(int idOrder) {
        this.idOrder = idOrder;
    }

    public int getIdListPrice() {
        return idListPrice;
    }

    public void setIdListPrice(int idListPrice) {
        this.idListPrice = idListPrice;
    }

    public int getIdArticle() {
        return idArticle;
    }

    public void setIdArticle(int idArticle) {
        this.idArticle = idArticle;
    }

    public int getIdBill() {
        return idBill;
    }

    public void setIdBill(int idBill) {
        this.idBill = idBill;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idOrder;
        hash += (int) idListPrice;
        hash += (int) idArticle;
        hash += (int) idBill;
        hash += (int) idUser;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrdersPK)) {
            return false;
        }
        OrdersPK other = (OrdersPK) object;
        if (this.idOrder != other.idOrder) {
            return false;
        }
        if (this.idListPrice != other.idListPrice) {
            return false;
        }
        if (this.idArticle != other.idArticle) {
            return false;
        }
        if (this.idBill != other.idBill) {
            return false;
        }
        if (this.idUser != other.idUser) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Models.Entities.OrdersPK[ idOrder=" + idOrder + ", idListPrice=" + idListPrice + ", idArticle=" + idArticle + ", idBill=" + idBill + ", idUser=" + idUser + " ]";
    }
    
}
