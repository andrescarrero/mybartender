package Models.Entities;

import Models.Entities.Article;
import Models.Entities.ListPricePK;
import Models.Entities.Orders;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-03-07T11:07:37")
@StaticMetamodel(ListPrice.class)
public class ListPrice_ { 

    public static volatile SingularAttribute<ListPrice, Date> dateini;
    public static volatile SingularAttribute<ListPrice, String> userCreate;
    public static volatile SingularAttribute<ListPrice, String> userEdit;
    public static volatile SingularAttribute<ListPrice, ListPricePK> listPricePK;
    public static volatile SingularAttribute<ListPrice, Float> price;
    public static volatile SingularAttribute<ListPrice, Date> datefin;
    public static volatile SingularAttribute<ListPrice, Date> dateCreate;
    public static volatile SingularAttribute<ListPrice, Date> dateEdit;
    public static volatile ListAttribute<ListPrice, Orders> ordersList;
    public static volatile SingularAttribute<ListPrice, Article> article;

}