package Models.Entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-03-07T11:07:37")
@StaticMetamodel(OrdersPK.class)
public class OrdersPK_ { 

    public static volatile SingularAttribute<OrdersPK, Integer> idUser;
    public static volatile SingularAttribute<OrdersPK, Integer> idOrder;
    public static volatile SingularAttribute<OrdersPK, Integer> idArticle;
    public static volatile SingularAttribute<OrdersPK, Integer> idListPrice;
    public static volatile SingularAttribute<OrdersPK, Integer> idBill;

}