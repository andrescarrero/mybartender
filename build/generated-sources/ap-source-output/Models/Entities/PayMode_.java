package Models.Entities;

import Models.Entities.Bill;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-03-07T11:07:37")
@StaticMetamodel(PayMode.class)
public class PayMode_ { 

    public static volatile SingularAttribute<PayMode, String> identificationCard;
    public static volatile SingularAttribute<PayMode, String> nameCard;
    public static volatile ListAttribute<PayMode, Bill> billList;
    public static volatile SingularAttribute<PayMode, String> cardType;
    public static volatile SingularAttribute<PayMode, String> securityCode;
    public static volatile SingularAttribute<PayMode, Integer> idPayMode;
    public static volatile SingularAttribute<PayMode, String> cardNumber;

}