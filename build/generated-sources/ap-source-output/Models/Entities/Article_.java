package Models.Entities;

import Models.Entities.ListPrice;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-03-07T11:07:37")
@StaticMetamodel(Article.class)
public class Article_ { 

    public static volatile SingularAttribute<Article, String> userCreate;
    public static volatile SingularAttribute<Article, Integer> idArticle;
    public static volatile SingularAttribute<Article, String> userEdit;
    public static volatile SingularAttribute<Article, String> name;
    public static volatile ListAttribute<Article, ListPrice> listPriceList;
    public static volatile SingularAttribute<Article, String> state;
    public static volatile SingularAttribute<Article, Integer> stock;
    public static volatile SingularAttribute<Article, Date> dateCreate;
    public static volatile SingularAttribute<Article, Date> dateEdit;
    public static volatile SingularAttribute<Article, String> urlImage;

}