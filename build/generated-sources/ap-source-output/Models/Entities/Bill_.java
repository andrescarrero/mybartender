package Models.Entities;

import Models.Entities.BillPK;
import Models.Entities.Client;
import Models.Entities.PayMode;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-03-07T11:07:37")
@StaticMetamodel(Bill.class)
public class Bill_ { 

    public static volatile SingularAttribute<Bill, Date> dateBill;
    public static volatile SingularAttribute<Bill, BillPK> billPK;
    public static volatile SingularAttribute<Bill, Client> client;
    public static volatile SingularAttribute<Bill, PayMode> idPayMode;

}