package Models.Entities;

import Models.Entities.Bill;
import Models.Entities.ListPrice;
import Models.Entities.OrdersPK;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-03-07T11:07:37")
@StaticMetamodel(Orders.class)
public class Orders_ { 

    public static volatile SingularAttribute<Orders, OrdersPK> ordersPK;
    public static volatile SingularAttribute<Orders, Integer> quantity;
    public static volatile SingularAttribute<Orders, Bill> bill;
    public static volatile SingularAttribute<Orders, ListPrice> listPrice;

}